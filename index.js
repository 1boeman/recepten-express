const config = require('./config');
const sqlite3 = require('sqlite3')
const express = require('express')
//import sqlite3 from 'sqlite3';
const SQLite3 = sqlite3.verbose();
const db = new SQLite3.Database(config.database);

const app = express()
const port = 3000


function select(query,param) {
  let par = param || []; 
  return new Promise(function(resolve, reject) {
    db.all(query,par,(err,rows) => { if (err) { reject(err) } else { resolve(rows)}  });
  });
}

app.use(express.static(config.frontend));

app.get('/all', (req, res) => {
  select('select id,titel from recepten').then(
    rows => res.send(rows),
    error => res.send(error.message)
  );
});


app.get('/item/:id', (req, res) => {
  let id  = req.params["id"];
  select("select r.* from recepten r where id = $id", {$id:id} ).then(
    rows => res.send(rows),
    error => res.send(error.message)
  );
});



app.listen(port, () => {
  console.log(`app listening on port ${port}`)
})


