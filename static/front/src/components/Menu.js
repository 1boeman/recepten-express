import React from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Menu = (props) => {
    const { menuData, setItem } = props; 
    
    return (
        <>
          <Row>
              <Col>
                <h1>Recepten</h1>
              </Col>
          </Row>
                { menuData.map (function(item,i){
                    return <MenuRow item={item} key={i} setItem={setItem} /> 
                })}
        </>
    );
}

const MenuRow = (props) => {
    const { item, setItem } = props;
    const menuItemSelect = function (e) {
        setItem(e.target.id);
    }
        
    return (
        <Row>
            <Col>
              <h2><a id={item.id} 
                    href={'#' + item.id}>{item.titel}</a></h2>
            </Col>
        </Row>
    );

};


export default Menu; 
