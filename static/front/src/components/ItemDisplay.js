import React from "react";
import ReactMarkdown from 'react-markdown'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const ItemDisplay = (props) => {
  const { itemData } = props; 
  
  return (
      <>
        <Row id={itemData.id}>
            <Col>
              <h1>{ itemData.titel }</h1>
            </Col>
        </Row>
        <Row>
            <Col>
              <h2>Ingrediënten:</h2>
              <div>
                  <ReactMarkdown children={itemData.ingredienten} />
              </div>
              <h2>Bereiding:</h2>
              <div>
                  <ReactMarkdown children={itemData.bereiding} />
              </div>
            </Col>
        </Row>
      </>
  );
}

export default ItemDisplay;
