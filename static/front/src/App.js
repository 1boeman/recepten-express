const config_file = 'config-' + process.env.NODE_ENV;
const config = require('./' + config_file);
import React, { useState, useEffect } from "react";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Menu from './components/Menu';
import ItemDisplay from './components/ItemDisplay';

const useHash = () => {
  const [hash, setHash] = React.useState(() => window.location.hash);
  const hashChangeHandler = React.useCallback((e) => {
    e.preventDefault();
    setHash(window.location.hash);
  }, []);

  React.useEffect(() => {
    window.addEventListener('hashchange', hashChangeHandler);
    return () => {
      window.removeEventListener('hashchange', hashChangeHandler);
    };
  }, []);

  const updateHash = React.useCallback(
    newHash => {
      if (newHash !== hash) window.location.hash = newHash;
    },
    [hash]
  );
  return [hash, updateHash];
};

const App = () => {
    const [menuData, setMenuData ] = useState([]);
    const server = config.server;
    const [ item, setItem ] = useState(0);
    const [ itemData, setItemData ] = useState([]);
    const [hash, setHash] = useHash();

    useEffect(()=>{
      let hash_i = hash.replace('#','');
      if (hash_i.length == 0) hash_i = 0;
      setItemData([])
      setItem(hash_i);
    },[hash]);
    
    useEffect(()=>{
      window.scrollTo(0,0);
    });
    
    useEffect(() => {
        fetch(server + "/all")
        .then((response) => response.json())
        .then((data) => setMenuData(data))
        .catch((error) => {
            alert("An error occurred!");
        });
    },[]);
    
    useEffect(()=>{
       if (item){
           fetch (server + "/item/" + item)
            .then((response) => response.json())
            .then((data) =>{ setItemData(data);  console.log(data) })
            .catch((error) => {
                alert("An error occurred!");
            });
       }
    },[item]);

   
    if (itemData.length) {
        return (
            <Container>
               <ItemDisplay itemData = { itemData[0] } /> 
            </Container>
        );
    } 

    return ( 
        <Container>
            {
                menuData.length
                ?   <Menu menuData={menuData} setItem={setItem} />
                :   <Row><Col>Waiting...</Col></Row>
            }
        </Container> 
    );
};

export default App;

