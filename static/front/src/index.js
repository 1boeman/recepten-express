import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'
import 'bootstrap/dist/css/bootstrap.min.css';

console.log(process.env.NODE_ENV)
ReactDOM.render(
  <App />,
  document.getElementById('app')
);
